{ config, lib, pkgs, ... }:

let
  cfg = config.services.yammat;
in
{
  options.services.yammat = with lib; {
    enable = mkEnableOption "Yammat";

    package = mkPackageOption pkgs "yammat" { } // {
      default = import ./. { inherit pkgs; };
    };

    user = mkOption {
      type = types.str;
      default = "yammat";
      description = "System user to run Yammat";
    };

    group = mkOption {
      type = types.str;
      default = "yammat";
      description = "System group to run Yammat";
    };

    config = mkOption {
      type = types.lines;
      description = "Configuration for Yammat";
    };
  };

  config = lib.mkIf cfg.enable {
    environment.etc."yammat.yml" = {
      enable = true;
      text = cfg.config;
    };

    services = {
      postgresql = {
        enable = true;
        ensureDatabases = [ "yammat" ];
        ensureUsers = [ {
          name = cfg.user;
          ensureDBOwnership = true;
        } ];
      };

      yammat.config = builtins.readFile ./config/settings.yml + /* yaml */ ''
        sendmail-location: "/run/wrappers/bin/sendmail"
      '';
    };

    systemd.services.yammat = {
      description = "Yammat Matemat";
      after = [ "postgresql.service" ];
      requires = [ "postgresql.service" ];
      wantedBy = [ "multi-user.target" ];
      environment = {
        APPROOT = cfg.package;
        STATIC_DIR = "${cfg.package}/static";
        PGHOST = "";
      };
      path = [ "/run/wrappers" ];
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        WorkingDirectory = cfg.package;
        ExecStart = "${lib.getExe cfg.package} /etc/yammat.yml";
      };
    };

    users = {
      groups.${cfg.group} = {};
      users.${cfg.user} = {
        group = cfg.group;
        isSystemUser = true;
      };
    };
  };
}
